﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingMachine.Exceptions;

namespace VendingMachine
{
  public struct Money : IComparable
  {
    public int Euros { get; set; }
    public int Cents { get; set; }


    public int CompareTo(object obj)
    {
      var m = (Money)obj;

      if (m.Euros < Euros)
        return 1;

      if (m.Euros == Euros && m.Cents < Cents)
        return 1;

      if (m.Euros == Euros && m.Cents == Cents)
        return 0;

      return -1;
    }

    public bool IsGreaterThan(Money other)
    {
      return CompareTo(other) > 0;
    }

    public static Money operator +(Money m1, Money m2)
    {
      var result = new Money();
      var cents = m1.Cents + m2.Cents;
      result.Euros = m1.Euros + m2.Euros + (cents / 100);
      result.Cents = cents % 100;

      return result;
    }

    public static Money operator -(Money m1, Money m2)
    {
      if (m2.IsGreaterThan(m1))
        throw new MinuendIsLessThanSubtrahendException();

      var result = new Money();
      result.Euros = m1.Euros - m2.Euros;

      if (m1.Cents < m2.Cents)
      {
        result.Euros = result.Euros - 1;
        result.Cents = 100 - (m2.Cents - m1.Cents);
      }
      else
      {
        result.Cents = m1.Cents - m2.Cents;
      }

      return result;
    }

    public override string ToString()
    {
      return string.Format("{0} Euros and {1} Cents", Euros, Cents);
    }
  }
}
