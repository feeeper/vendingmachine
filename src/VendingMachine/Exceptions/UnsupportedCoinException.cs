﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Exceptions
{
  public class UnsupportedCoinException : Exception
  {
    private string _message;

    public override string Message
    {
      get
      {
        return _message;
      }
    }

    public UnsupportedCoinException(Money coin)
    {
      _message =
        string.Format("Coin {0} is not supported. Vending machine accepts only 5ȼ, 10ȼ, 20ȼ, 50ȼ, 1€ and 2€ coins", coin);
    }

  }



}