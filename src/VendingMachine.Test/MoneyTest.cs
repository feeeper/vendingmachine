﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Exceptions;

namespace VendingMachine.Test
{
  [TestClass]
  public class MoneyTest
  {
    [TestMethod]
    public void Comparing_2Euros10CentsGreaterThan1Euro10Cents_ReturnTrue()
    {
      var m1 = new Money() { Cents = 10, Euros = 2 };
      var m2 = new Money() { Cents = 10, Euros = 1 };

      Assert.IsTrue(m1.IsGreaterThan(m2));
    }

    [TestMethod]
    public void Comparing_1Euros20CentsGreaterThan1Euro10Cents_ReturnTrue()
    {
      var m1 = new Money() { Cents = 20, Euros = 1 };
      var m2 = new Money() { Cents = 10, Euros = 1 };

      Assert.IsTrue(m1.IsGreaterThan(m2));
    }



    [TestMethod]
    public void Sum_1Euro1CentAddedTo9Euros9Cents_Return10Euros10Cents()
    {
      var m1 = new Money() { Cents = 9, Euros = 9 };
      var m2 = new Money() { Cents = 1, Euros = 1 };

      Assert.AreEqual(new Money() { Cents = 10, Euros = 10 }, m1 + m2);
    }

    [TestMethod]
    public void Sum_1Euro1CentAddedTo1Euro99Cents_Return3Euros()
    {
      var m1 = new Money() { Cents = 99, Euros = 1 };
      var m2 = new Money() { Cents = 1, Euros = 1 };

      Assert.AreEqual(new Money() { Cents = 0, Euros = 3 }, m1 + m2);
    }

    [TestMethod]
    public void Sum_1Euro99CentsAddedTo1000Euros99Cents_Return1002Euros98Cents()
    {
      var m1 = new Money() { Cents = 99, Euros = 1 };
      var m2 = new Money() { Cents = 99, Euros = 1000 };
      var expected = new Money() { Cents = 98, Euros = 1002 };
      var actual = m1 + m2;

      Assert.AreEqual(expected, actual, 
        string.Format("Expected 1002 Euros and 98 Cents. Actual {0} Euros and {1} Cents", actual.Euros, actual.Cents));
    }

    [TestMethod]
    public void Subtraction_1Euro1CentSubtractedFrom10Euros10Cents_Returns9Euros9Cents()
    {
      var m1 = new Money() { Cents = 1, Euros = 1 };
      var m2 = new Money() { Cents = 10, Euros = 10 };
      var expected = new Money() { Cents = 9, Euros = 9 };
      var actual = m2 - m1;

      Assert.AreEqual(expected, actual,
        string.Format("Expected 9 Euros and 9 Cents. Actual {0} Euros and {1} Cents", actual.Euros, actual.Cents));
    }

    [TestMethod]
    public void Subtraction_1Euro20CentsSubtractedFrom10Euros10Cents_Returns8Euros90Cents()
    {
      var m1 = new Money() { Cents = 20, Euros = 1 };
      var m2 = new Money() { Cents = 10, Euros = 10 };
      var expected = new Money() { Cents = 90, Euros = 8 };
      var actual = m2 - m1;

      Assert.AreEqual(expected, actual,
        string.Format("Expected 8 Euros and 90 Cents. Actual {0} Euros and {1} Cents", actual.Euros, actual.Cents));
    }

    [TestMethod]
    [ExpectedException(typeof (MinuendIsLessThanSubtrahendException))]
    public void Subtraction_1Euro10CentsSubtractedFrom1Euro_MinuendIsLessThanSubtrahendExceptionThrown()
    {
      var m1 = new Money() { Cents = 10, Euros = 1 };
      var m2 = new Money() { Euros = 1 };
      var actual = m2 - m1;
    }
  }
}
