﻿using System.Linq;
using VendingMachine.Exceptions;

namespace VendingMachine
{
  public class VendingMachine : IVendingMachine
  {
    private Money _amount;
    
    private Money[] _supportedCoins = new Money[6]
    {
      new Money() { Cents = 5 },
      new Money() { Cents = 10 },
      new Money() { Cents = 20 },
      new Money() { Cents = 50 },
      new Money() { Euros = 1 },
      new Money() { Euros = 2 }
    };

    private bool _customerBoughtProduct = false;

    public string Manufacturer { get; }
    
    public Money Amount
    {
      get { return _amount; }
    }

    public Product[] Products { get; set; }

    public Money InsertCoin(Money amount)
    {
      if (!_supportedCoins.Contains(amount))
        throw new UnsupportedCoinException(amount);

      _amount += amount;
      return _amount;
    }

    public Money ReturnMoney()
    {
      var result = _amount;
      // There are no money in the machine after money is returned to customer.
      _amount = new Money();
      return result;
    }
    
    public Product Buy(int productNumber)
    {
      // You can buy 1 product at once for inserted coins
      if (_customerBoughtProduct)
        throw new MultiplePurchasesNotSupportedException();

      if (Products.Length < productNumber || Products[productNumber].Available <= 0)
        throw new ProductNotAvailableException();

      var productToBuy = Products[productNumber];

      if (productToBuy.Price.IsGreaterThan(Amount))
        throw new NotEnoughMoneyException(string.Format("Product's price is {0}. Amount is {1}. Please insert more coins.",
          productToBuy.Price,
          Amount));
      
      // Product is a struct (valuable type) so updating productToBuy don't updates real product in the machine.
      Products[productNumber].Available--;

      // Available money have to decrease after each purchase.
      _amount -= productToBuy.Price;

      _customerBoughtProduct = true;

      return productToBuy;
    }
  }
}
