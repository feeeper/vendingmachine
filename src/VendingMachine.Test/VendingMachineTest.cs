﻿using System.Linq;
using System.Runtime.Remoting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Exceptions;

namespace VendingMachine.Test
{
  [TestClass]
  public class VendingMachineTest
  {
    VendingMachine _vendingMachine;

    [TestInitialize]
    public void Init()
    {
      _vendingMachine = new VendingMachine();
    }

    [TestMethod]
    [ExpectedException(typeof(ProductNotAvailableException))]
    public void Buy_LessThanOneProductCount_ProductNotAvailableExceptionThrown()
    {
      _vendingMachine.Products = Enumerable.Empty<Product>().ToArray();
      var product = _vendingMachine.Buy(1);
    }

    [TestMethod]
    [ExpectedException(typeof(NotEnoughMoneyException))]
    public void Buy_NotEnoughMoneyForProduct_NotEnoughMoneyExceptionThrown()
    {
      _vendingMachine.Products = new Product[1];
      _vendingMachine.Products[0] = new Product()
      {
        Available = 1,
        Name = "apple",
        Price = new Money()
        {
          Cents = 0,
          Euros = 1
        }
      };

      _vendingMachine.Buy(0);
    }

    [TestMethod]
    public void InsertCoin_Insert1EuroCoins_AmountIs1Euro()
    {
      _vendingMachine.InsertCoin(new Money() { Cents = 0, Euros = 1 });
      Assert.AreEqual(new Money() { Cents = 0, Euros = 1 }, _vendingMachine.Amount);
    }

    [TestMethod]
    public void InsertCoin_Insert10EuroCoin10Times_AmountIs1Euro()
    {
      for (var i = 0; i < 10; i++)
      {
        _vendingMachine.InsertCoin(new Money() { Cents = 10, Euros = 0 });
      }
      Assert.AreEqual(new Money() { Cents = 0, Euros = 1 }, _vendingMachine.Amount);
    }

    [TestMethod]
    [ExpectedException(typeof(UnsupportedCoinException))]
    public void InsertCoin_Insert15CentsCoin_UnsupportedCoinExceptionThrown()
    {
      _vendingMachine.InsertCoin(new Money() { Cents = 15, Euros = 0 });
    }

    [TestMethod]
    public void Buy_WithoutInsertedMoney_NotThrown() { }

    [TestMethod]
    public void Buy_HaveEnoughMoneyForProduct_ReturnProductAndDecreaseAvailableProductsCount()
    {
      _vendingMachine.Products = new Product[1];
      _vendingMachine.Products[0] = new Product()
      {
        Available = 1,
        Name = "apple",
        Price = new Money()
        {
          Euros = 1
        }
      };
      _vendingMachine.InsertCoin(new Money() { Euros = 1 });

      var product = _vendingMachine.Buy(0);

      Assert.AreEqual("apple", product.Name);
      Assert.AreEqual(0, _vendingMachine.Products[0].Available);
    }

    [TestMethod]
    public void Buy_InsertMuchMoneyThanProductCosts_ReturnProductAndRemainder()
    {
      _vendingMachine.Products = new Product[1];
      _vendingMachine.Products[0] = new Product()
      {
        Available = 1,
        Name = "apple",
        Price = new Money()
        {
          Cents = 10
        }
      };
      _vendingMachine.InsertCoin(new Money() { Euros = 1 });

      var productToBuy = _vendingMachine.Buy(0);

      Assert.AreEqual(new Money() { Cents = 90 }, _vendingMachine.Amount);

      var remainder = _vendingMachine.ReturnMoney();

      Assert.AreEqual(new Money() { Cents = 90 }, remainder);
      Assert.AreEqual(new Money(), _vendingMachine.Amount);
    }

    [TestMethod]
    [ExpectedException(typeof (MultiplePurchasesNotSupportedException))]
    public void Buy_MultipleProductsAtOnceInsertedCoins_MultiplePurchasesNotSupportedExceptionThrown()
    {
      _vendingMachine.Products = new Product[1];
      _vendingMachine.Products[0] = new Product()
      {
        Available = 2,
        Name = "apple",
        Price = new Money()
        {
          Cents = 10
        }
      };
      _vendingMachine.InsertCoin(new Money() { Euros = 2 });

      var productToBuy = _vendingMachine.Buy(0);
      var productToBuy1 = _vendingMachine.Buy(0);
    }

    [TestMethod]
    [ExpectedException(typeof (ProductNotAvailableException))]
    public void Buy_NotEnoughProductAvailable_ProductNotAvailableExceptionThrown()
    {
      _vendingMachine.Products = new Product[1];
      _vendingMachine.Products[0] = new Product()
      {
        Available = 0,
        Name = "apple",
        Price = new Money()
        {
          Cents = 10
        }
      };
      _vendingMachine.InsertCoin(new Money() { Euros = 2 });

      var productToBuy = _vendingMachine.Buy(0);
    }
  }
}
